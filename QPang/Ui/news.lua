--[[ All Location Ver.]]
--2012/04/04 NL News.lua Update Ver.

News_NLD = {
--총 9개의 공지이미지

-- Promotion : Stoer Magazine
	   Notice01 = {
       img = "notice-48.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- 1st Person Point & 3rd Person Point
	Notice02 = {
       img = "notice-44.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Enchant
       Notice03 = {
       img = "notice-42.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Emoticon
       Notice04 = {
       img = "notice-41.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Resolution
       Notice05 = {
       img = "notice-38.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Dancing
       Notice06 = {
       img = "notice-39.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Control
       Notice07 = {
       img = "notice-26.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Manual
       Notice08 = {
       img = "notice-00.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },


-- Screenshot
       Notice09 = {
       img = "notice-25.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },
}

News_GER = {
--총 10개의 공지이미지

-- Sitdown System Guide
	   Notice01 = {
       img = "notice-13.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- 1st Person Point & 3rd Person Point
       Notice02 = {
       img = "notice-11.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Enchant
       Notice03 = {
       img = "notice-09.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Emoticon
       Notice04 = {
       img = "notice-07.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- ItemCrane
       Notice05 = {
       img = "notice-05.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Resolution
       Notice06 = {
       img = "notice-04.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Dancing
       Notice07 = {
       img = "notice-03.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Control
       Notice08 = {
       img = "notice-02.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Manual
       Notice09 = {
       img = "notice-01.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },
}

News_USA = {
--총 8개의 공지이미지

-- Abount Enchant System
       Notice01 = {
       img = "notice-09.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Cowboy Clothes Add
       Notice02 = {
       img = "notice-08.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Emoticon Use Guide
       Notice03 = {
       img = "notice-07.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- PVP & PVE Guide
       Notice04 = {
       img = "notice-06.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Item Crane Guide
       Notice05 = {
       img = "notice-05.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Resolution Upgrade
       Notice06 = {
       img = "notice-04.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Shall We Dance
       Notice07 = {
       img = "notice-03.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Ctrl Key Use Guide
       Notice08 = {
       img = "notice-02.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },

-- Basic Control Key Guide
       Notice09 = {
       img = "notice-00.png",
                
        buttons = {
            --btn0 = { cmd = "open_cardshop",},
            --btn1 = { cmd = "open_guide",},        
        },
    },
}